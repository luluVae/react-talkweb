import React from 'react'
import { Route, Switch } from "react-router-dom"
// import Page404 from '../../../../pages/404/404'

export default class Test2page extends React.Component {

	testpage = () => {
  		this.props.history.push(this.props.match.url + '/test3page')
  	}

	render() {
	return (
		<div>
	    	<div onClick={ this.testpage }> test2page </div>
	    	<Switch>
	    		{
	    			this.props.subRouters.map((r, key) => {
	    				const Component = r.component,
                      		subRouter = this.props.allRouters.filter(item => item.parent === r.name)
	    				return <Route key={ key }  render={ props => <Component { ...props } allRouters={ this.props.allRouters } subRouter={ subRouter } /> } exact={ r.exact } path={ this.props.match.url + r.path } />
	    			})
	    		}
	    		{/*<Route component={ Page404 } />*/}
	    	</Switch>
		</div>
	)
	}
}