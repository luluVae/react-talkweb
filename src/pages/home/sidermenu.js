import React from 'react'
import { Menu } from 'antd'

export default class Sidermenu extends React.Component {
  switchRouter = path => {
    this.props.history.push(this.props.match.url + path)
  }
  siderSubmenu = (content, item) => {
    const subContent = content.filter(contentItem => contentItem.parentId === item.id)
    if (subContent.length === 0) {
      return (
        <Menu.Item key={ item.id } onClick={ this.switchRouter.bind(this, item.to) }>
          <i className={ `fa fa-${item.icon}` }></i> <span>{ item.label }</span>
        </Menu.Item>
      )
    }
    return (
      <Menu.SubMenu key={ item.id } title={ <span><i className={ `fa fa-${item.icon}` }></i> { item.label }</span> }>
        { subContent.map(child => (
          this.siderSubmenu(content, child)
          )) }
      </Menu.SubMenu>
    )
  }
  render() {
    const content = this.props.content
    return (
      <Menu theme="dark" style={ { background: '#2f4050' } } mode="inline" inlineIndent={ 10 }>
        { content.filter(item => item.parentId === '#').map(item => (
          this.siderSubmenu(content.filter(contentItem => contentItem.parentId !== '#'), item)
          )) }
      </Menu>
    )
  }
}